#!/usr/bin/env bash

function get_latest_master_tag() { git ls-remote git@gitlab.com:gitlab-org/professional-services-automation/tools/migration/congregate.git | grep refs/heads/master | cut -f 1; }

function get_congregate_pids() { ps aux | grep "congregate" | awk '{print $2}' | grep -v grep; }

alias penv='poetry install'
alias ui='(cd frontend && npm install)'
alias sh='poetry shell'
alias upd='poetry run python congregate/update-readme.py'
alias pt='poetry run pytest \
            -m "unit_test" \
            --cov-report term-missing \
            --cov-report html \
            --cov-config=.coveragerc \
            --cov=congregate congregate/tests/'
alias pt_e2e='poetry run pytest -s -m e2e congregate/tests/'
alias dpull='docker pull registry.gitlab.com/gitlab-org/professional-services-automation/tools/migration/congregate:rolling-debian'
alias drun='docker run \
              --name congregate \
              -v /var/run/docker.sock:/var/run/docker.sock \
              -v /etc/hosts:/etc/hosts \
              -v $HOME/congregate_data:/opt/congregate/data \
              -v $HOME/congregate_downloads:/opt/congregate/downloads \
              -p 8000:8000 \
              -it registry.gitlab.com/gitlab-org/professional-services-automation/tools/migration/congregate:rolling-debian \
              /bin/bash'
alias pyclean='find ./congregate -name "*.pyc" -delete'
alias gendocs='poetry run mkdocs serve -a 0.0.0.0:8080'
alias kc='kill $(get_congregate_pids)'
alias kc9='kill -9 $(get_congregate_pids)'
alias wave_archive='dev/bin/wave_archive.sh'
alias ggit='./dev/bin/generic_git.sh'

printf '# pyproject.toml and Dockerfile aliases\n'
alias penv
alias ui
alias sh
alias upd
alias pt
alias pt_e2e
printf '\n# docker pull latest docker image on master\n'
alias dpull
printf '\n# docker run latest docker image on master\n'
alias drun
alias pyclean
alias gendocs
alias kc
alias kc9
printf '\n# wave setup and archiving management\n'
alias wave_archive
printf '\n# Gerrit -> Git - Input source repo SSH/HTTPS clone url such as "ggit ssh://<username>@<server_name>:29418/<repo_name>.git"'
alias ggit
