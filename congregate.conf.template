# Congregate configuration settings
#! This file is generated during initial configuration and **is not** modified
#! during upgrades.


## Once you have installed congregate, You can run `./congregate.sh -h` to display the list of commands you can use.
## And to configure congregate, run `./congregate.sh configure`
## This command would let you configure your source and destination hosts and their details, export options, user details, app details etc
##! Locally, the complete configuration file can be found at: congregate/data/congregate.conf
################################################################################
################################################################################
##                Congregate Configuration Settings                           ##
################################################################################
################################################################################
################################################################################
## congregate.conf configuration
##! Docs: https://gitlab.com/gitlab-org/professional-services-automation/tools/migration/congregate/-/blob/master/README.md
################################################################################
##! In general, the values specified here should reflect what the default value of the attribute will be.
##! There are instances where this behavior is not possible or desired. For example, when providing passwords,
##! or connecting to third party services.
##! In those instances, we endeavour to provide an example configuration.
##! Token and password values must be obfuscated. You can run `./congregate.sh obfuscate` to mask them.
###! **Configuration settings below are for Destination host**
[DESTINATION]
### General configuration for GitLab server
dstn_hostname = hostname
dstn_access_token = *****
import_user_id = 0
# shared_runners_enabled = False
# project_suffix = False
# max_import_retries = 3
# dstn_parent_group_id = 0
# dstn_parent_group_path = destination_parent_group_full_path
# group_sso_provider = group_sso_provider
# group_sso_provider_pattern = email
# username_suffix = migrated
# mirror_username =
### User field to search and map by
# user_mapping_field = email
### The maximum number of hours to rollback users, groups, and projects. Default value is 24
# max_asset_expiration_time = 24
# dstn_registry_url = destination_registry_url

### There are few other additional configurations for Destination host which are used sometimes based on requirements received
### append_project_suffix_on_existing_found setting determines if, in the instance of an existing project being found at the destination with the
### same name as the source project, if we should append a value and create the project, or just fail the import.
### The value from the append_project_suffix configuration value, or `False` by default
# append_project_suffix_on_existing_found = False

### Consider few changes in the configuration when the source is either Bitbucket or Github server,
### Rest all remains same as mentioned in General configuration
# import_user_id = 0
# shared_runners_enabled = False
# project_suffix = False
# max_import_retries = 3
# username_suffix = migrated
### Following Destination changes based on reporting requirements and issue creation
# reporting = {
#    "post_migration_issues": ["issue1.md", "issue2.md"],
#    "pmi_project_id": 6439,
#    "subs": {
#        "faq_page": "https://about.gitlab.com/handbook/engineering/infrastructure/faq/",
#        "jira_page": "https://www.atlassian.com/"
#        }
#    }
###! **HIDDEN PROPERTIES**
# lower_case_group_path = False
# lower_case_project_path = False
# users_to_ignore = []
###! **Configuration settings below are for Source host**
[SOURCE]
### General configuration for GitLab server
### This configuration is considered when the source is a GitLab server
src_type = GitLab
src_hostname = source_hostname
src_access_token = *****
# src_tier = core
# src_parent_group_id = 0
# src_parent_group_path = source_group_full_path
# src_registry_url = source_registry_url

### Consider below changes when the source is a Bitbucket server, rest all other configuration remains same.
### The token should be the obfuscated user account password.
# src_type = Bitbucket Server
# src_username = username
# src_hostname = some_external_source
# src_access_token = *****

### Consider below changes when the source is a GitHub server, rest all other configuration remains same
# src_type = GitHub
# src_hostname = some_external_source
# src_access_token = *****
### src_password is only used by the GitHubBrowser class. Use an access token for any API requests.
# src_password = ****
### src_parent_org is used when migrating a single organization e.g. from github.com
# src_parent_org = src_parent_org
### Consider below changes when multiple sources are GitHub Server, all other configuration remains same
#sources = {
#    "github_source": [{
#        "src_hostname": "https://github.example1.net",
#        "src_access_token": "<base64 encoded token>"
#        },{
#        "src_hostname": "https://github.example2.net",
#        "src_access_token": "<base64 encoded token>"
#        }
#    ]
#    }
###! **Configuration settings below are for CI Source**
### Consider below changes when the source is a CI server like Jenkins, rest all other configuration remains same
#sources = {
#     "jenkins_ci_source": [
#         {
#             "jenkins_ci_src_hostname": "http://52.177.135.31:8080"
#             "jenkins_ci_src_username": "jenkins",
#             "jenkins_ci_src_access_token": "MTFlNjBhYTY4MjEwODkzOGQ0MDdhNmQyMzcwM2RlZjdkYw==",
#         },
#         {
#             "jenkins_ci_src_hostname": "http://52.177.135.31:8080",
#             "jenkins_ci_src_username": "jenkins",
#             "jenkins_ci_src_access_token": "<token>"
#         },
#         {
#             "jenkins_ci_src_hostname": "http://52.177.135.31:8080",
#             "jenkins_ci_src_username": "jenkins",
#             "jenkins_ci_src_access_token": "MTFlNjBhYTY4MjEwODkzOGQ0MDdhNmQyMzcwM2RlZjdkYw=="
#         }
#     ],
#     "teamcity_ci_source": [
#             {
#             "tc_ci_src_hostname": "http://13.92.25.192",
#             "tc_ci_src_username": "test",
#             "tc_ci_src_access_token": "ZXlKMGVYQWlPaUFpVkVOV01pSjkuT1RsMk1uQldjVGhIZURaelRVOVRWR3hvTmsxM2VrdzBTRmhSLllUSmtPRGc1TkRJdFpqRm1OeTAwTnpnekxXRXhNRGt0T0Roa1lUWmxNVFprT1RNMw=="
#             }
#         ]
#     }
###! **HIDDEN PROPERTIES**
### Full .txt file path. List of URLs (per line), used with --subset list command argument, to list a custom subset of BitBucket projects or repos.
# list_subset_input_path =
###! **Configuration settings below are for EXPORT**
[EXPORT]
### When exporting groups/projects to your local filesystem (default) the following fields are required:
location = filesystem
filesystem_path = /absolute_path

### It is also possible to configure export to AWS (S3 bucket). This ONLY applies to project exports.
### Consider the below configuration when there is a need to export to AWS:
# location = aws
# s3_name = s3_name
# s3_region = us-east-2
# s3_access_key_id = *****
# s3_secret_access_key = dGVzdA==
# filesystem_path = /absolute_path

###! **HIDDEN PROPERTIES**
### Used only by disabled migration mode "filesystem-aws"
# allow_presigned_url = False
###! **Configuration settings below are for USER**
[USER]
### Consider below configuration if you need to keep inactive users,
### whether or not you should send the reset password link on user creation
### or to generate a random password at create
# keep_inactive_users = True
### NOTE: Either reset_pwd or force_rand_password need to be set to True for users to be created
# reset_pwd = False
# force_rand_pwd = True

###! **HIDDEN PROPERTIES**
### Used only by "map-users" command
# user_map_csv =
# projects_limit =
# skip_keys_migration = False

###! **Configuration settings below are for APP**
[APP]
### The frequency (in seconds) of checking a group or project export or import status.
### We can adjust it (10 by default) depending whether we are migrating during peak hours or not.
### In general it should be increased when using multiple processes i.e. when the API cannot handle all the requests.
export_import_status_check_time = 10

### The maximum amount of time (in seconds) to wait for exports or imports. It differs for local filesystem and AWS
### Consider "3600" for local filesystem and "1200" for AWS
### The export part is only applicable to GL->GL migrations
export_import_timeout = 3600

### Presents the Slack Incoming Webhooks URL for sending alerts (logs) to a dedicated GitLab internal private channel.
### Optionally used during customer migrations, mainly to gitlab.com, but also an option for migrations to self-managed.
# slack_url = https://slack.url

### Optional configuration if you are using a spreadsheet to keep track of migration waves
### Specify an absolute path to the spreadsheet. Supports excel, openoffice, and csv spreadsheet formats
# wave_spreadsheet_path = /absolute/path/to/waves.xls
### Specify the column names for congregate to use from the spreadsheet. These names will be required to the right of the colon in the next configuration item.
# wave_spreadsheet_columns = Migration wave, Date of execution, Repo URL, Group Path, Override
### If you are using columns that do not match the expected "Wave name", "Wave date", and "Source Url", you will need to provide a map setting like the example below
### Make sure the dictionary mappings are written either in a single line, e.g.
# wave_spreadsheet_column_mapping = {"Wave name": "Migration wave", "Wave date": "Date of execution", "Source Url": "Repo URL", "Parent Path": "Group Path", "Override": "Override"}
### Or, for better readability, in space/tab indented new lines, with the closing bracket part of the last line/mapping, e.g.
# wave_spreadsheet_column_mapping = {
#        "Wave name": "Migration wave",
#        "Wave date": "Date of execution",
#        "Source Url": "Repo URL",
#        "Parent Path": "Group Path",
#        "Override": "Override"}
### Mongo DB configuration
mongo_host = localhost
mongo_port = 27017

### Redis configuration - for running air-gapped migrations
# redis_host = localhost
# redis_port = 6379

### gRPC configuration - for migrating package registries (Maven)
# grpc_host = localhost
# maven_port = 50051
### Setting to enforce SSL Verification with our API requests. Defaults to True
ssl_verify = True

### Default number of parallel processes
processes = 4

### The port used to serve up the flask/VueJS UI
ui_port = 8000

###! **HIDDEN PROPERTIES**
### Path to a JSON file for remapping URLs during a GitLab to GitLab import. Notes:
# remapping_file_path =
