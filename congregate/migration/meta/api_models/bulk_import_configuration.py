from dataclasses import dataclass

@dataclass
class BulkImportconfiguration:
    url: str
    access_token: str